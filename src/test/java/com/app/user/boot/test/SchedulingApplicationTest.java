package com.app.user.boot.test;

import javax.transaction.Transactional;

import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.app.spring.boot.time.SchedulingApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SchedulingApplication.class)
@FixMethodOrder(MethodSorters.JVM)
@Transactional
public class SchedulingApplicationTest {

	
}
