package com.app.api.response;

import java.util.Map;

public class ApiResponse {
	
	private int statusCode;
	
	private String status;
	
	private String message;
	
	private Map<String,Object> details;
	
	public ApiResponse(){
		super();
	}
	
	public ApiResponse(int statusCode, String status,String message, Map<String, Object> details) {
		super();
		this.statusCode = statusCode;
		this.status = status;
		this.message = message;
		this.details = details;
	}
	/**
	 * @return the statusCode
	 */
	public int getStatusCode() {
		return statusCode;
	}

	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the details
	 */
	public Map<String, Object> getDetails() {
		return details;
	}
	/**
	 * @param details the details to set
	 */
	public void setDetails(Map<String, Object> details) {
		this.details = details;
	}
}
