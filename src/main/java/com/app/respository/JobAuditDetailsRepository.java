package com.app.respository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.entity.JobAuditDetails;

@Repository
public interface JobAuditDetailsRepository extends JpaRepository<JobAuditDetails, Long> {
    

}