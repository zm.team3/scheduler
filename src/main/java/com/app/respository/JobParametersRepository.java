package com.app.respository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.entity.JobParameters;

@Repository
public interface JobParametersRepository extends JpaRepository<JobParameters, Long> {
	/**
	 * This method will return the job object based on the active/inactive parameter
	 * @param isActive
	 * @return
	 */
	JobParameters findByJobStatus(Boolean isActive);
	
	/**
	 * This method will return the job object based on the active/inactive parameter and service name
	 * @param isActive
	 * @param serviceName
	 * @return
	 */
	JobParameters findByJobStatusAndServiceName(Boolean isActive,String serviceName);
	
	/**
	 * This method will return the job object based on the active/inactive parameter and service code
	 * @param isActive
	 * @param serviceName
	 * @return
	 */
	JobParameters findByJobStatusAndServiceCode(Boolean isActive,String serviceCode);

	/**
	 * This method will be used to find the object based on the run id and service name
	 * @param runId
	 * @param serviceName
	 * @return
	 */
	JobParameters findByRunIdAndServiceName(Long runId, String serviceName);

	/**
	 * This method will be used to find the object based on the run id
	 * @param runId
	 * @return
	 */
	JobParameters findByRunId(Long runId);

	/**
	 * This method will be used to find the object based on the service name
	 * @param serviceName
	 * @return
	 */
	JobParameters findByServiceName(String serviceName);
}