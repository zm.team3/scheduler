package com.app.respository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.entity.JobServices;

@Repository
public interface JobServicesRepository extends JpaRepository<JobServices, Long> {
    
	JobServices findByServiceName(String serviceName);

	JobServices findByServiceCode(String applicationRolesService);
	
	List<JobServices> findByIsActive(Boolean isActive);
	
}