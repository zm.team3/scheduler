package com.app.service.impl;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.api.response.ApiResponse;
import com.app.constant.AppConstant;
import com.app.entity.JobAuditDetails;
import com.app.entity.JobParameters;
import com.app.entity.JobServices;
import com.app.respository.JobAuditDetailsRepository;
import com.app.respository.JobParametersRepository;
import com.app.respository.JobServicesRepository;
import com.app.service.JobService;

@Service
public class JobServiceImpl implements JobService {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private JobParametersRepository jobParametersRepository;
	
	@Autowired
	private JobAuditDetailsRepository auditDetailsRepository;
	
	@Autowired
	private JobServicesRepository jobServicesRepository;
	
	/**
	 * This method will return the job object based on the active/inactive parameter
	 * @param isActive
	 * @return
	 */
	@Override
	public JobParameters getJobParameterStatus(Boolean isActive){
		JobParameters jobParameters = null;
		try{
			jobParameters = jobParametersRepository.findByJobStatus(isActive);
		}catch(Exception exception){
			exception.printStackTrace();
			jobParameters = null;
		}
		return jobParameters;
	}
	
	/**
	 * This method will return the job object based on the active/inactive parameter and service name
	 * @param isActive
	 * @param serviceName
	 * @return
	 */
	@Override
	public JobParameters getJobParameterStatusAndServiceName(Boolean isActive,String serviceName){
		JobParameters jobParameters = null;
		try{
			jobParameters = jobParametersRepository.findByJobStatusAndServiceName(isActive,serviceName);
		}catch(Exception exception){
			exception.printStackTrace();
			jobParameters = null;
		}
		return jobParameters;
	}
	
	/**
	 * This method will return the job object based on the active/inactive parameter and service code
	 * @param isActive
	 * @param serviceCode
	 * @return
	 */
	public JobParameters getJobParameterStatusAndServiceCode(Boolean isActive,String serviceCode){
		JobParameters jobParameters = null;
		try{
			jobParameters = jobParametersRepository.findByJobStatusAndServiceCode(isActive,serviceCode);
		}catch(Exception exception){
			exception.printStackTrace();
			jobParameters = null;
		}
		return jobParameters;
	}
	
	/**
	 * This method will be used to save the Job parameters when request is coming from the form
	 * @param jobParameters
	 * @return
	 */
	@Override
	public ApiResponse saveJobParameters(JobParameters jobParameters){
		ApiResponse apiResponse = null;
		try{
			//daily frequency
			if(jobParameters.getId()==null){
				jobParameters.setCreatedAt(Instant.now());	
//				Timestamp jobStartDate = jobParameters.getJobStartDate();
//				jobStartDate.setHours(Integer.parseInt(jobParameters.getJobHours()));
//				jobStartDate.setMinutes(Integer.parseInt(jobParameters.getJobMinutes()));
//				jobParameters.setNextJobRunDate(jobStartDate);
			}else{
//				JobParameters parameters = jobParametersRepository.findById(jobParameters.getId()).orElse(null);
//				if(parameters.getJobFrequency().equalsIgnoreCase(jobParameters.getJobFrequency())){
//					jobParameters.setNextJobRunDate(parameters.getNextJobRunDate());
//				}else{
//					Timestamp jobStartDate = jobParameters.getJobStartDate();
//					jobStartDate.setHours(Integer.parseInt(jobParameters.getJobHours()));
//					jobStartDate.setMinutes(Integer.parseInt(jobParameters.getJobMinutes()));
//					jobParameters.setNextJobRunDate(jobStartDate);
//				}
				jobParameters.setUpdatedAt(Instant.now());
			}
			Timestamp jobStartDate = jobParameters.getJobStartDate();
			jobStartDate.setHours(Integer.parseInt(jobParameters.getJobHours()));
			jobStartDate.setMinutes(Integer.parseInt(jobParameters.getJobMinutes()));
			jobParameters.setNextJobRunDate(jobStartDate);
			
			jobParameters = jobParametersRepository.save(jobParameters);
			Map<String,Object> finalMap = new LinkedHashMap<>();
			finalMap.put("jobParameters", jobParameters);
			apiResponse = new ApiResponse(AppConstant.SUCCESS_CODE,AppConstant.STATUS_SUCCESS,AppConstant.SUCCESS_MESSAGE,null);
		}catch(Exception exception){
			exception.printStackTrace();
			apiResponse = new ApiResponse(AppConstant.INTERNAL_SERVER_ERROR,AppConstant.STATUS_FAILURE,AppConstant.INTERNAL_SERVER_ERROR_MESSAGE,null);
		}
		return apiResponse;
	}
	
	/**
	 * This method will be used to get the job parameter details 
	 * @param jobParameters
	 * @return
	 */
	@Override
	public List<JobParameters> getJobDetails(){
		return jobParametersRepository.findAll();
	}
	
	/**
	 * This method will be used to get the service name based on the run id or service name
	 * @param runId
	 * @param serviceName
	 * @return
	 */
	@Override
	public JobParameters getJobParameterDetailsById(Long runId,String serviceName){
		JobParameters jobParameters = null;
		try{
			if(runId!=null && serviceName!=null){
				jobParameters =  jobParametersRepository.findByRunIdAndServiceName(runId,serviceName);
			}else if(runId!=null){
				jobParameters =  jobParametersRepository.findByRunId(runId);
			}else if(serviceName!=null){
				jobParameters =  jobParametersRepository.findByServiceName(serviceName);
			}
		}catch(Exception exception){
			exception.printStackTrace();
			jobParameters = null;
		}
		return jobParameters;
	}
	/**
	 * This method will be used to find all the audit log details
	 * @return
	 */
	@Override
	public List<JobAuditDetails> getJobAuditLogs(){
		return auditDetailsRepository.findAll();
	}
	
	/**
	 * This method will be used to find the details of the job run
	 * @return
	 */
	public JobAuditDetails getJobAuditLogDetails(Long jobId){
		return auditDetailsRepository.findById(jobId).orElse(null);
	}
	
	/**
	 * This method will be used to find all the services available for the job
	 * @return
	 */
	public List<JobServices> getAllJobServices(Boolean isActive){
		return jobServicesRepository.findByIsActive(isActive);
	}
}
