package com.app.service;

import java.util.List;

import com.app.api.response.ApiResponse;
import com.app.entity.JobAuditDetails;
import com.app.entity.JobParameters;
import com.app.entity.JobServices;

public interface JobService {
	
	/**
	 * This method will return the job object based on the active/inactive parameter
	 * @param isActive
	 * @return
	 */
	public JobParameters getJobParameterStatus(Boolean isActive);
	
	/**
	 * This method will return the job object based on the active/inactive parameter and service name
	 * @param isActive
	 * @param serviceName
	 * @return
	 */
	public JobParameters getJobParameterStatusAndServiceName(Boolean isActive,String serviceName);
	
	/**
	 * This method will return the job object based on the active/inactive parameter and service code
	 * @param isActive
	 * @param serviceCode
	 * @return
	 */
	public JobParameters getJobParameterStatusAndServiceCode(Boolean isActive,String serviceCode);
	
	/**
	 * This method will be used to save the Job parameters when request is coming from the form
	 * @param jobParameters
	 * @return
	 */
	public ApiResponse saveJobParameters(JobParameters jobParameters);
	
	/**
	 * This method will be used to get the job parameter details 
	 * @param jobParameters
	 * @return
	 */
	public List<JobParameters> getJobDetails();
	
	/**
	 * This method will be used to get the service name based on the run id or service name
	 * @param runId
	 * @param serviceName
	 * @return
	 */
	public JobParameters getJobParameterDetailsById(Long runId,String serviceName);
	
	/**
	 * This method will be used to find all the audit log details
	 * @return
	 */
	public List<JobAuditDetails> getJobAuditLogs();

	/**
	 * This method will be used to find the details of the job run
	 * @return
	 */
	public JobAuditDetails getJobAuditLogDetails(Long jobId);

	/**
	 * This method will be used to find all the services available for the job
	 * @return
	 */
	public List<JobServices> getAllJobServices(Boolean isActive);
}
