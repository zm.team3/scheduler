package com.app.entity;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.app.entity.audit.DateAudit;

@Entity
@Table(name = "JOB_PARAMETERS")
public class JobParameters extends DateAudit {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long runId;
    
    private Timestamp jobStartDate;
    
    private String jobHours;
    
    private String jobMinutes;

    private Boolean jobStatus = true;//default is true
    
    private String jobFrequency;
    
    private Timestamp jobEndDate;
    
    private Timestamp nextJobRunDate;
    
    private String serviceName;
    
    private String serviceCode;
    
    private String runDays;

    public JobParameters(){
    	super();
    }
    
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the runId
	 */
	public Long getRunId() {
		return runId;
	}

	/**
	 * @param runId the runId to set
	 */
	public void setRunId(Long runId) {
		this.runId = runId;
	}

	/**
	 * @return the jobHours
	 */
	public String getJobHours() {
		return jobHours;
	}

	/**
	 * @param jobHours the jobHours to set
	 */
	public void setJobHours(String jobHours) {
		this.jobHours = jobHours;
	}

	/**
	 * @return the jobMinutes
	 */
	public String getJobMinutes() {
		return jobMinutes;
	}

	/**
	 * @param jobMinutes the jobMinutes to set
	 */
	public void setJobMinutes(String jobMinutes) {
		this.jobMinutes = jobMinutes;
	}

	/**
	 * @return the jobStatus
	 */
	public Boolean getJobStatus() {
		return jobStatus;
	}

	/**
	 * @param jobStatus the jobStatus to set
	 */
	public void setJobStatus(Boolean jobStatus) {
		this.jobStatus = jobStatus;
	}

	/**
	 * @return the jobFrequency
	 */
	public String getJobFrequency() {
		return jobFrequency;
	}

	/**
	 * @param jobFrequency the jobFrequency to set
	 */
	public void setJobFrequency(String jobFrequency) {
		this.jobFrequency = jobFrequency;
	}

	/**
	 * @return the jobStartDate
	 */
	public Timestamp getJobStartDate() {
		return jobStartDate;
	}

	/**
	 * @param jobStartDate the jobStartDate to set
	 */
	public void setJobStartDate(Timestamp jobStartDate) {
		this.jobStartDate = jobStartDate;
	}

	/**
	 * @return the jobEndDate
	 */
	public Timestamp getJobEndDate() {
		return jobEndDate;
	}

	/**
	 * @param jobEndDate the jobEndDate to set
	 */
	public void setJobEndDate(Timestamp jobEndDate) {
		this.jobEndDate = jobEndDate;
	}

	/**
	 * @return the nextJobRunDate
	 */
	public Timestamp getNextJobRunDate() {
		return nextJobRunDate;
	}

	/**
	 * @param nextJobRunDate the nextJobRunDate to set
	 */
	public void setNextJobRunDate(Timestamp nextJobRunDate) {
		this.nextJobRunDate = nextJobRunDate;
	}

	/**
	 * @return the serviceName
	 */
	public String getServiceName() {
		return serviceName;
	}

	/**
	 * @param serviceName the serviceName to set
	 */
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	/**
	 * @return the runDays
	 */
	public String getRunDays() {
		return runDays;
	}

	/**
	 * @param runDays the runDays to set
	 */
	public void setRunDays(String runDays) {
		this.runDays = runDays;
	}

	/**
	 * @return the serviceCode
	 */
	public String getServiceCode() {
		return serviceCode;
	}

	/**
	 * @param serviceCode the serviceCode to set
	 */
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
}