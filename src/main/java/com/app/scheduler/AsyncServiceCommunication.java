package com.app.scheduler;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.app.constant.AppConstant;
import com.app.entity.JobAuditDetails;
import com.app.entity.JobParameters;
import com.app.entity.JobServices;
import com.app.respository.JobAuditDetailsRepository;
import com.app.respository.JobParametersRepository;
import com.app.respository.JobServicesRepository;
import com.app.service.JobService;
import com.app.valueobject.ApplicationRoles;
import com.app.valueobject.ApplicationRolesDetails;
import com.app.valueobject.EmailResponse;
import com.app.valueobject.EmailVO;
import com.app.valueobject.Package;
import com.app.valueobject.PackageDetails;
import com.app.valueobject.Profile;
import com.app.valueobject.ProfileDetails;
import com.app.valueobject.Specialization;
import com.app.valueobject.SpecializationDetails;

@Component
public class AsyncServiceCommunication {

	@Autowired
	private JobService jobService; 
	
	@Autowired
	private JobParametersRepository jobParametersRepository; 
	
	@Autowired
	private JobAuditDetailsRepository jobAuditDetailsRepository; 
	
	@Autowired
	private JobServicesRepository jobServicesRepository;
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${mail.toList}")
	private String toMailList;
	
	@Value("${mail.subject}")
	private String subject;
	
	@Value("${mail.body}")
	private String body;
	
	/**
	 * This method is used to validate and call the subscription service
	 * @throws Exception 
	 */
	@Async("processExecutor")
	public void validateAndExecuteService(String serviceCode) throws Exception {
		//check the job status whether it is active or not
		JobParameters jobParameters = jobService.getJobParameterStatusAndServiceCode(true,serviceCode);
		Boolean isValidationSuccess = validateJob(jobParameters);
		//if success makes a service call
		if(isValidationSuccess){
		//if(true){
			String description = "";
			JobAuditDetails auditDetails = new JobAuditDetails();
			auditDetails.setCreatedAt(Instant.now());
			auditDetails.setStatus(AppConstant.STATUS_SUCCESS);
			auditDetails.setRunId(jobParameters.getRunId());
			auditDetails.setServiceName(jobParameters.getServiceName());
			try{
				JobServices jobServices = jobServicesRepository.findByServiceCode(serviceCode);
				if(jobServices!=null){
					//Application Role service
					if(serviceCode.equalsIgnoreCase(AppConstant.APPLICATION_ROLES_SERVICE)){
						ApplicationRoles applicationRoles = restTemplate.getForObject(jobServices.getServiceURL(),ApplicationRoles.class);
						if(applicationRoles!=null){
							List<ApplicationRolesDetails> roleList = applicationRoles.getContent();
							description = AppConstant.SERVICE_SUCCESS.concat(" and number of role list is :").concat(roleList!=null?String.valueOf(roleList.size()):"0");
						}else{
							description = AppConstant.SERVICE_FAILURE;
						}
					}else if(serviceCode.equalsIgnoreCase(AppConstant.PACKAGE_ALL_SERVICE)){//package service
						Package package1 = restTemplate.getForObject(jobServices.getServiceURL(),Package.class);
						if(package1!=null){
							List<PackageDetails> packageList = package1.getContent();
							description = AppConstant.SERVICE_SUCCESS.concat(" and number of package list is :").concat(packageList!=null?String.valueOf(packageList.size()):"0");
						}else{
							description = AppConstant.SERVICE_FAILURE;
						}
					}else if(serviceCode.equalsIgnoreCase(AppConstant.SPECIALIZATIONS_SERVICE)){//specialization service
						Specialization specialization = restTemplate.getForObject(jobServices.getServiceURL(),Specialization.class);
						if(specialization!=null){
							List<SpecializationDetails> specializationList = specialization.getContent();
							description = AppConstant.SERVICE_SUCCESS.concat(" and number of specialization list is :").concat(specializationList!=null?String.valueOf(specializationList.size()):"0");
						}else{
							description = AppConstant.SERVICE_FAILURE;
						}
					}else if(serviceCode.equalsIgnoreCase(AppConstant.PROFILE_TYPE_SERVICE)){//profile type service
						Profile profile = restTemplate.getForObject(jobServices.getServiceURL(),Profile.class);
						if(profile!=null){
							List<ProfileDetails> profileList = profile.getContent();
							description = AppConstant.SERVICE_SUCCESS.concat(" and number of specialization list is :").concat(profileList!=null?String.valueOf(profileList.size()):"0");
						}else{
							description = AppConstant.SERVICE_FAILURE;
						}
					}
				}else{
					description = AppConstant.APPLICATION_ROLES_SERVICE.concat(" url is not defined in the configuration table.Please check and provide the same");
				}
			}catch(Exception exception){
				auditDetails.setStatus(AppConstant.STATUS_FAILURE);
				description = description.concat(exception.getCause().toString());
//				auditDetails.setDescription(description.length()>3999 ?description.substring(0,3998):description);
			}finally{
				//mail service
				JobServices emailService = jobServicesRepository.findByServiceCode("SEND_EMAIL");
				List<String> addressList = Arrays.asList(toMailList);
				EmailVO emailVO = new EmailVO(addressList,subject,body);
				try{
					EmailResponse emailResponse = restTemplate.postForObject(emailService.getServiceURL(), emailVO, EmailResponse.class); //email service calls
					description = description.concat(" and email response from the service is : "+emailResponse.getContent());
				}catch(Exception exception){
					description = description.concat(" and error occured while sending the email : "+exception.getCause().toString());
				}
				description = description.length()>3999 ?description.substring(0,3998):description;
				auditDetails.setDescription(description);
				jobAuditDetailsRepository.save(auditDetails);
			}
		}
	}
	
	/**
	 * This method is used to validate the job whether needs to be executed or not
	 * @param jobParameters
	 * @return
	 * @throws Exception 
	 */
	private Boolean validateJob(JobParameters jobParameters) throws Exception{
		Boolean isValidationSuccess = false;
		//if the job status is active then only we need to process the job
		if(jobParameters!=null){
			Boolean isActive = jobParameters.getJobStatus();
			Timestamp jobEndDate = jobParameters.getJobEndDate();
			Date endDate = new Date(jobEndDate.getTime());  
			Timestamp currentDate = new Timestamp(System.currentTimeMillis());
			currentDate.setHours(currentDate.getHours()+5);
			currentDate.setMinutes(currentDate.getMinutes()+30);
			if(isActive && (currentDate.before(endDate) || currentDate.equals(endDate))){
				//job date
				Timestamp jobRunDate = jobParameters.getNextJobRunDate();
				Date nextJobRunDate = new Date(jobRunDate.getTime());  
				int jobHours = 0;
				int jobMinutes = 0;
				//job times
				if(jobParameters.getJobFrequency().equalsIgnoreCase(AppConstant.MINUTES_FREQUENCY_15) || jobParameters.getJobFrequency().equalsIgnoreCase(AppConstant.MINUTES_FREQUENCY_30)){
					jobHours = jobRunDate.getHours();
					jobMinutes = jobRunDate.getMinutes();
				}else{
					jobHours = Integer.parseInt(jobParameters.getJobHours());
					jobMinutes = Integer.parseInt(jobParameters.getJobMinutes());
				}
//				int jobHours = Integer.parseInt(jobParameters.getJobHours());
//				int jobMinutes = Integer.parseInt(jobParameters.getJobMinutes());
				nextJobRunDate.setHours(jobHours);
				nextJobRunDate.setMinutes(jobMinutes);

				if(currentDate.after(nextJobRunDate)){
					//next job run date after calculation
					jobParameters.setNextJobRunDate(new Timestamp(nextJobRunDate.getTime()));
					Timestamp tempTimestamp = jobParameters.getNextJobRunDate();
					if(jobParameters.getJobFrequency().equalsIgnoreCase(AppConstant.MINUTES_FREQUENCY_15)){
						tempTimestamp.setMinutes(tempTimestamp.getMinutes()+Integer.parseInt(AppConstant.MINUTES_FREQUENCY_15));
						jobParameters.setNextJobRunDate(tempTimestamp);
					}
					if(jobParameters.getJobFrequency().equalsIgnoreCase(AppConstant.MINUTES_FREQUENCY_30)){
						tempTimestamp.setMinutes(tempTimestamp.getMinutes()+Integer.parseInt(AppConstant.MINUTES_FREQUENCY_30));
						jobParameters.setNextJobRunDate(tempTimestamp);
					}
					//daily frequency
					if(jobParameters.getJobFrequency().equalsIgnoreCase(AppConstant.DAILY_FREQUENCY)){
						jobParameters.setNextJobRunDate(addDays(AppConstant.DAILY_FREQUENCY_INCREMENT,jobParameters.getNextJobRunDate()));
					}//weekly frequency
					if(jobParameters.getJobFrequency().equalsIgnoreCase(AppConstant.WEEKLY_FREQUENCY)){
						String actualDay = (new SimpleDateFormat("EEEE")).format(currentDate.getTime()).toUpperCase(); // "Tuesday"
						String runDays = jobParameters.getRunDays();
						if(runDays!=null){
							Map<String,String> days = new LinkedHashMap<>();
							String[] runDaysArray = runDays.split(",");
							for (int i = 0; i < runDaysArray.length; i++) {
								days.put(runDaysArray[i],runDaysArray[i]);
							}
							Integer counter = 0;
							if(days.containsKey(actualDay) && "MONDAY".equalsIgnoreCase(actualDay)){
								counter = findNextCounterMonday(days);
							}
							if(days.containsKey(actualDay) && "TUESDAY".equalsIgnoreCase(actualDay)){
								counter = findNextCounterTuesday(days);							
							}
							if(days.containsKey(actualDay) && "WEDNESDAY".equalsIgnoreCase(actualDay)){
								counter = findNextCounterWednesday(days);
							}
							if(days.containsKey(actualDay.toUpperCase()) && "THURSDAY".equalsIgnoreCase(actualDay)){
								counter = findNextCounterTHURSDAY(days);
							}
							if(days.containsKey(actualDay) && "FRIDAY".equalsIgnoreCase(actualDay)){
								counter = findNextCounterFriday(days);
							}
							if(days.containsKey(actualDay) && "SATURDAY".equalsIgnoreCase(actualDay)){
								counter = findNextCounterSaturday(days);							
							}
							if(days.containsKey(actualDay) && "SUNDAY".equalsIgnoreCase(actualDay)){
								counter = findNextCounterSunday(days);
							}
							System.out.println("=======counter===33333======"+counter);
							jobParameters.setNextJobRunDate(addDays(counter,jobParameters.getNextJobRunDate()));
						}else{
							jobParameters.setNextJobRunDate(addDays(AppConstant.WEEKLY_FREQUENCY_INCREMENT,jobParameters.getNextJobRunDate()));
						}
					}//monthly frequency
					if(jobParameters.getJobFrequency().equalsIgnoreCase(AppConstant.MONTHLY_FREQUENCY)){
						jobParameters.setNextJobRunDate(addDays(AppConstant.MONTHLY_FREQUENCY_INCREMENT,jobParameters.getNextJobRunDate()));
					}
					jobParametersRepository.save(jobParameters);
					isValidationSuccess = true;
				}
			}
		}
		return isValidationSuccess;
	}
	/**
	 * This method will be used to calculate the number of days
	 * @param days
	 * @return
	 */
	private Long dayToMiliseconds(int days){
	    Long result = Long.valueOf(days * 24 * 60 * 60 * 1000);
	    return result;
	}

	/**
	 * this method will add the number of days in current timestamp
	 * @param days
	 * @param t1
	 * @return
	 * @throws Exception
	 */
	public Timestamp addDays(int days, Timestamp t1) throws Exception{
	    if(days < 0){
	        throw new Exception("Day in wrong format.");
	    }
	    Long miliseconds = dayToMiliseconds(days);
	    return new Timestamp(t1.getTime() + miliseconds);
	}
	
	/**
	 * This method will be used to calculate the increment of number of days based on the weekly frequency
	 * @param days
	 * @return
	 */
	private Integer findNextCounterMonday(Map<String,String> days){
		Integer counter = 0;
		Boolean isPassed = true;
		if(isPassed && days.containsKey("TUESDAY")){
			counter = 1;
			isPassed = false;
		}
		if(isPassed && days.containsKey("WEDNESDAY")){
			counter = 2;
			isPassed = false;
		}
		if(isPassed && days.containsKey("THURSDAY")){
			counter = 3;
			isPassed = false;
		}
		if(isPassed && days.containsKey("FRIDAY")){
			counter = 4;
			isPassed = false;
		}
		if(isPassed && days.containsKey("SATURDAY")){
			counter = 5;
			isPassed = false;
		}
		if(isPassed && days.containsKey("SUNDAY")){
			counter = 6;
			isPassed = false;
		}
		if(isPassed && days.containsKey("MONDAY")){
			counter = 7;
			isPassed = false;
		}
		return counter;
	}
	/**
	 * This method will be used to calculate the increment of number of days based on the weekly frequency
	 * @param days
	 * @return
	 */
	private Integer findNextCounterTuesday(Map<String,String> days){
		Integer counter = 0;
		Boolean isPassed = true;
		
		if(isPassed && days.containsKey("WEDNESDAY")){
			counter = 1;
			isPassed = false;
		}
		if(isPassed && days.containsKey("THURSDAY")){
			counter = 2;
			isPassed = false;
		}
		if(isPassed && days.containsKey("FRIDAY")){
			counter = 3;
			isPassed = false;
		}
		if(isPassed && days.containsKey("SATURDAY")){
			counter = 4;
			isPassed = false;
		}
		if(isPassed && days.containsKey("SUNDAY")){
			counter = 5;
			isPassed = false;
		}
		if(isPassed && days.containsKey("MONDAY")){
			counter = 6;
			isPassed = false;
		}
		if(isPassed && days.containsKey("TUESDAY")){
			counter = 7;
			isPassed = false;
		}
		return counter;
	}
	/**
	 * This method will be used to calculate the increment of number of days based on the weekly frequency
	 * @param days
	 * @return
	 */
	private Integer findNextCounterWednesday(Map<String,String> days){
		Integer counter = 0;
		Boolean isPassed = true;
		
		if(isPassed && days.containsKey("THURSDAY")){
			counter = 1;
			isPassed = false;
		}
		if(isPassed && days.containsKey("FRIDAY")){
			counter = 2;
			isPassed = false;
		}
		if(isPassed && days.containsKey("SATURDAY")){
			counter = 3;
			isPassed = false;
		}
		if(isPassed && days.containsKey("SUNDAY")){
			counter = 4;
			isPassed = false;
		}
		if(isPassed && days.containsKey("MONDAY")){
			counter = 5;
			isPassed = false;
		}
		if(isPassed && days.containsKey("TUESDAY")){
			counter = 6;
			isPassed = false;
		}
		if(isPassed && days.containsKey("WEDNESDAY")){
			counter = 7;
			isPassed = false;
		}
		return counter;
	}
	/**
	 * This method will be used to calculate the increment of number of days based on the weekly frequency
	 * @param days
	 * @return
	 */
	private Integer findNextCounterTHURSDAY(Map<String,String> days){
		Integer counter = 0;
		Boolean isPassed = true;
		
		if(isPassed && days.containsKey("FRIDAY")){
			counter = 1;
			isPassed = false;
		}
		if(isPassed && days.containsKey("SATURDAY")){
			counter = 2;
			isPassed = false;
		}
		if(isPassed && days.containsKey("SUNDAY")){
			counter = 3;
			isPassed = false;
		}
		if(isPassed && days.containsKey("MONDAY")){
			counter = 4;
			isPassed = false;
		}
		if(isPassed && days.containsKey("TUESDAY")){
			counter = 5;
			isPassed = false;
		}
		if(isPassed && days.containsKey("WEDNESDAY")){
			counter = 6;
			isPassed = false;
		}
		if(isPassed && days.containsKey("THURSDAY")){
			counter = 7;
			isPassed = false;
		}
		return counter;
	}
	/**
	 * This method will be used to calculate the increment of number of days based on the weekly frequency
	 * @param days
	 * @return
	 */
	private Integer findNextCounterFriday(Map<String,String> days){
		Integer counter = 0;
		Boolean isPassed = true;
		
		if(isPassed && days.containsKey("SATURDAY")){
			counter = 1;
			isPassed = false;
		}
		if(isPassed && days.containsKey("SUNDAY")){
			counter = 2;
			isPassed = false;
		}
		if(isPassed && days.containsKey("MONDAY")){
			counter = 3;
			isPassed = false;
		}
		if(isPassed && days.containsKey("TUESDAY")){
			counter = 4;
			isPassed = false;
		}
		if(isPassed && days.containsKey("WEDNESDAY")){
			counter = 5;
			isPassed = false;
		}
		if(isPassed && days.containsKey("THURSDAY")){
			counter = 6;
			isPassed = false;
		}
		if(isPassed && days.containsKey("FRIDAY")){
			counter = 7;
			isPassed = false;
		}
		return counter;
	}
	/**
	 * This method will be used to calculate the increment of number of days based on the weekly frequency
	 * @param days
	 * @return
	 */
	private Integer findNextCounterSaturday(Map<String,String> days){
		Integer counter = 0;
		Boolean isPassed = true;
		
		if(isPassed && days.containsKey("SUNDAY")){
			counter = 1;
			isPassed = false;
		}
		if(isPassed && days.containsKey("MONDAY")){
			counter = 2;
			isPassed = false;
		}
		if(isPassed && days.containsKey("TUESDAY")){
			counter = 3;
			isPassed = false;
		}
		if(isPassed && days.containsKey("WEDNESDAY")){
			counter = 4;
			isPassed = false;
		}
		if(isPassed && days.containsKey("THURSDAY")){
			counter = 5;
			isPassed = false;
		}
		if(isPassed && days.containsKey("FRIDAY")){
			counter = 6;
			isPassed = false;
		}
		if(isPassed && days.containsKey("SATURDAY")){
			counter = 7;
			isPassed = false;
		}
		return counter;
	}
	/**
	 * This method will be used to calculate the increment of number of days based on the weekly frequency
	 * @param days
	 * @return
	 */
	private Integer findNextCounterSunday(Map<String,String> days){
		Integer counter = 0;
		Boolean isPassed = true;
		if(isPassed && days.containsKey("MONDAY")){
			counter = 1;
			isPassed = false;
		}
		if(isPassed && days.containsKey("TUESDAY")){
			counter = 2;
			isPassed = false;
		}
		if(isPassed && days.containsKey("WEDNESDAY")){
			counter = 3;
			isPassed = false;
		}
		if(isPassed && days.containsKey("THURSDAY")){
			counter = 4;
			isPassed = false;
		}
		if(isPassed && days.containsKey("FRIDAY")){
			counter = 5;
			isPassed = false;
		}
		if(isPassed && days.containsKey("SATURDAY")){
			counter = 6;
			isPassed = false;
		}
		if(isPassed && days.containsKey("SUNDAY")){
			counter = 7;
			isPassed = false;
		}
		return counter;
	}
}
