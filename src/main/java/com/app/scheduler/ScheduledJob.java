package com.app.scheduler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.app.constant.AppConstant;

@Component
public class ScheduledJob {
	
	/** async communication calls */
	@Autowired
	private AsyncServiceCommunication asyncServiceCommunication;
	
	@Scheduled(fixedDelay=60*1000*2, initialDelay=60*1000*2)
//	@Scheduled(fixedDelay=60*1000*2)
	public void scheduleFixedDelayTask() throws Exception {
		
		//APPLICATION_ROLES_SERVICE service calls
		asyncServiceCommunication.validateAndExecuteService(AppConstant.APPLICATION_ROLES_SERVICE);
		
		//PACKAGE_ALL_SERVICE service calls
		asyncServiceCommunication.validateAndExecuteService(AppConstant.PACKAGE_ALL_SERVICE);
		
		//SPECIALIZATIONS_SERVICE service calls
		asyncServiceCommunication.validateAndExecuteService(AppConstant.SPECIALIZATIONS_SERVICE);
		
		//PROFILE_TYPE_SERVICE service calls
		asyncServiceCommunication.validateAndExecuteService(AppConstant.PROFILE_TYPE_SERVICE);
	}
}
