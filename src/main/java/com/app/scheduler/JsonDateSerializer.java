package com.app.scheduler;

import java.io.IOException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class JsonDateSerializer extends JsonSerializer<Instant> {

	private ZoneId zone = ZoneId.of("Asia/Kolkata");
	private DateTimeFormatter fmt = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss").withZone(zone);
	
	@Override
	public void serialize(Instant date, JsonGenerator gen, SerializerProvider provider)throws IOException, JsonProcessingException {
		try {
			String str = fmt.format(date);
	        gen.writeString(str);
		} catch (Exception exception) {
		}
	}
}
