package com.app.constant;

public class AppConstant {
	
	public static final int INTERNAL_SERVER_ERROR = 500;
	public static final String INTERNAL_SERVER_ERROR_MESSAGE="Error while saving/updaing the data...!!!";
	
	public static final int SUCCESS_CODE = 200;
	public static final String SUCCESS_MESSAGE="Job parameters saved successfully...!!!";

//	public static final String SUBSCRIPTION_SERVICE = "subscription";
//	public static final String USER_PROFILE_SERVICE = "userProfile";
	
	public static final String APPLICATION_ROLES_SERVICE = "APPLICATION_ROLES";
	public static final String PACKAGE_ALL_SERVICE = "PACKAGE_ALL";
	public static final String SPECIALIZATIONS_SERVICE = "SPECIALIZATIONS";
	public static final String PROFILE_TYPE_SERVICE = "PROFILE_TYPE";
	public static final String SEND_EMAIL_SERVICE = "SEND_EMAIL";
	
	public static final String DAILY_FREQUENCY = "Daily";
	public static final int DAILY_FREQUENCY_INCREMENT = 1;
	
	public static final String MONTHLY_FREQUENCY = "Monthly";
	public static final int MONTHLY_FREQUENCY_INCREMENT = 30;
	
	public static final String WEEKLY_FREQUENCY = "Weekly";
	public static final int WEEKLY_FREQUENCY_INCREMENT = 7;
	
	public static final String MINUTES_FREQUENCY_15 = "15";
	public static final String MINUTES_FREQUENCY_30 = "30";
	
	public static final String STATUS_SUCCESS = "SUCCESS";
	public static final String STATUS_FAILURE = "FAILURE";
	
	public static final String SERVICE_SUCCESS = "Service connected and response received...";
	
	public static final String SERVICE_FAILURE = "Service connected but no response received...";
}
