package com.app.valueobject;

import java.io.Serializable;

public class PackageDetails implements Serializable{
	
	public int id;
	
	public String price;
    
	public String currency;
    
	public String perUserPrice;
    
    public String packageName;
    
    public Object offer;
	
	public PackageDetails(){
		super();
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the price
	 */
	public String getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(String price) {
		this.price = price;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the perUserPrice
	 */
	public String getPerUserPrice() {
		return perUserPrice;
	}

	/**
	 * @param perUserPrice the perUserPrice to set
	 */
	public void setPerUserPrice(String perUserPrice) {
		this.perUserPrice = perUserPrice;
	}

	/**
	 * @return the packageName
	 */
	public String getPackageName() {
		return packageName;
	}

	/**
	 * @param packageName the packageName to set
	 */
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	/**
	 * @return the offer
	 */
	public Object getOffer() {
		return offer;
	}

	/**
	 * @param offer the offer to set
	 */
	public void setOffer(Object offer) {
		this.offer = offer;
	}
}
