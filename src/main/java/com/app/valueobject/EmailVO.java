package com.app.valueobject;

import java.io.Serializable;
import java.util.List;

public class EmailVO implements Serializable{

	private List<String> toAddress;
	
	private String subject;
	
	private String body;
	
	public EmailVO(){
		super();
	}
	
	public EmailVO(List<String> toAddress, String subject, String body) {
		super();
		this.toAddress = toAddress;
		this.subject = subject;
		this.body = body;
	}

	/**
	 * @return the toAddress
	 */
	public List<String> getToAddress() {
		return toAddress;
	}

	/**
	 * @param toAddress the toAddress to set
	 */
	public void setToAddress(List<String> toAddress) {
		this.toAddress = toAddress;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the body
	 */
	public String getBody() {
		return body;
	}

	/**
	 * @param body the body to set
	 */
	public void setBody(String body) {
		this.body = body;
	}
}
