package com.app.valueobject;

import java.io.Serializable;

public class SpecializationDetails implements Serializable{
	
	public int specializationId;
	
    public String name;
	
	public SpecializationDetails(){
		super();
	}

	/**
	 * @return the specializationId
	 */
	public int getSpecializationId() {
		return specializationId;
	}

	/**
	 * @param specializationId the specializationId to set
	 */
	public void setSpecializationId(int specializationId) {
		this.specializationId = specializationId;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
}
