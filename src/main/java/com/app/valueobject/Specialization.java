package com.app.valueobject;

import java.io.Serializable;
import java.util.List;

public class Specialization implements Serializable {

	public List<SpecializationDetails> content;
    
	public String status;
    
	public Specialization(){
		super();
	}

	/**
	 * @return the content
	 */
	public List<SpecializationDetails> getContent() {
		return content;
	}



	/**
	 * @param content the content to set
	 */
	public void setContent(List<SpecializationDetails> content) {
		this.content = content;
	}



	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
}
