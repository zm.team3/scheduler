package com.app.valueobject;

import java.io.Serializable;
import java.util.List;

public class Package implements Serializable {

	public List<PackageDetails> content;
    
	public String status;
    
	public Package(){
		super();
	}

	/**
	 * @return the content
	 */
	public List<PackageDetails> getContent() {
		return content;
	}


	/**
	 * @param content the content to set
	 */
	public void setContent(List<PackageDetails> content) {
		this.content = content;
	}


	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
}
