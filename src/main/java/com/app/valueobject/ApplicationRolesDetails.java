package com.app.valueobject;

import java.io.Serializable;

public class ApplicationRolesDetails implements Serializable{

	 public int roleId;
	 
	 public String roleName;
	 
	 public String roleLabelName;
	 
	 public ApplicationRolesDetails(){
		 super();
	 }

	/**
	 * @return the roleId
	 */
	public int getRoleId() {
		return roleId;
	}

	/**
	 * @param roleId the roleId to set
	 */
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	/**
	 * @return the roleName
	 */
	public String getRoleName() {
		return roleName;
	}

	/**
	 * @param roleName the roleName to set
	 */
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	/**
	 * @return the roleLabelName
	 */
	public String getRoleLabelName() {
		return roleLabelName;
	}

	/**
	 * @param roleLabelName the roleLabelName to set
	 */
	public void setRoleLabelName(String roleLabelName) {
		this.roleLabelName = roleLabelName;
	}
}
