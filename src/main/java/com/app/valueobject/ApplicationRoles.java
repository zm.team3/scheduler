package com.app.valueobject;

import java.io.Serializable;
import java.util.List;

public class ApplicationRoles implements Serializable{

	public List<ApplicationRolesDetails> content;
	
    public String status;
    
    public ApplicationRoles(){
    	super();
    }

	/**
	 * @return the content
	 */
	public List<ApplicationRolesDetails> getContent() {
		return content;
	}

	/**
	 * @param content the content to set
	 */
	public void setContent(List<ApplicationRolesDetails> content) {
		this.content = content;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
    
    
    
}
