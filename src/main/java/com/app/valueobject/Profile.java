package com.app.valueobject;

import java.io.Serializable;
import java.util.List;

public class Profile implements Serializable {

	public List<ProfileDetails> content;
    
	public String status;
    
	public Profile(){
		super();
	}

	/**
	 * @return the content
	 */
	public List<ProfileDetails> getContent() {
		return content;
	}

	/**
	 * @param content the content to set
	 */
	public void setContent(List<ProfileDetails> content) {
		this.content = content;
	}




	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
}
