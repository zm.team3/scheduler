package com.app.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.api.response.ApiResponse;
import com.app.entity.JobAuditDetails;
import com.app.entity.JobParameters;
import com.app.entity.JobServices;
import com.app.service.JobService;

/**
 * This class will be act as a rest end points and will perform will the operations related to job activities
 *
 */
@RestController
@RequestMapping("/job")
public class JobController {

	@Autowired
	private JobService jobService;
	
	/**
	 * This end point will save the job parameter details which is coming from the screen
	 * @param user
	 * @return
	 */
	@PostMapping("/addJobParameter")
	public ApiResponse addJobParameter(HttpServletRequest request,@Valid @RequestBody JobParameters jobParameters){
		return jobService.saveJobParameters(jobParameters);
	}
	

	/**
	 * This method will be used to find the job parameter details
	 * @param request
	 * @return
	 */
	@GetMapping("/jobParametersDetails")
	public List<JobParameters> getJobDetails(HttpServletRequest request){
		return jobService.getJobDetails();
	}
	
	/**
	 * This method will be used to find the job parameter details
	 * @param request
	 * @return
	 */
	@GetMapping("/findByRunIdAndServiceName")
	public JobParameters getJobDetails(HttpServletRequest request,@RequestParam(value = "runId") Long runId,@RequestParam(value = "serviceName") String serviceName){
		return jobService.getJobParameterDetailsById(runId, serviceName);
	}
	
	/**
	 * This method will be used to find the job parameter details
	 * @param request
	 * @return
	 */
	@GetMapping("/findAudit")
	public List<JobAuditDetails> getJobAudit(HttpServletRequest request){
		return jobService.getJobAuditLogs();
	}
	
	/**
	 * This method will be used to find the details of the job run 
	 * @param request
	 * @return
	 */
	@GetMapping("/findAuditDetails")
	public JobAuditDetails getJobAuditDetails(HttpServletRequest request,@RequestParam(value = "jobId") Long jobId){
		return jobService.getJobAuditLogDetails(jobId);
	}
	
	/**
	 * This method will be used to find all the services available for the job
	 * @param request
	 * @return
	 */
	@GetMapping("/findActiveServices")
	public List<JobServices> getAllJobServices(HttpServletRequest request){
		return jobService.getAllJobServices(true);
	}
}