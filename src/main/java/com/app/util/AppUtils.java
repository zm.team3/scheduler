package com.app.util;

import java.util.UUID;

public class AppUtils {

	/**
	 * this method is used to generate the user ref when new user is getting created
	 * @return
	 */
	public static String generateUserRef(){
		UUID uniqueKey = UUID.randomUUID();
		return uniqueKey.toString();
	}
}
